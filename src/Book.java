import java.io.Serializable;

//file ready for static review

@SuppressWarnings("serial")
// RENAMED CLASS 'book' TO Book
public class Book implements Serializable {
	// RENAMED VARIABLE NAMES
	private String title;
	private String author;
	private String callNo;
	private int id;
	// RENAMED ON_LOAN TO ONLOAN
	private enum STATE { AVAILABLE, ONLOAN, DAMAGED, RESERVED };
	private STATE state;
	
	
	public Book(String author, String title, String callNo, int id) {
		this.author = author;
		this.title = title;
		this.callNo = callNo;
		this.id = id;
		this.state = STATE.AVAILABLE;
	}
		
	// RENAMED 'book' TO Book and UPDATED VARIABLE NAMES
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Book: ").append(id).append("\n")
		  .append("  Title:  ").append(title).append("\n")
		  .append("  Author: ").append(author).append("\n")
		  .append("  CallNo: ").append(callNo).append("\n")
		  .append("  State:  ").append(state);
		
		return sb.toString();
	}

	// RENAMED METHOD TO LOWER CASE
	public Integer id() {
		return id;
	}
	
	// RENAMED METHOD TO LOWER CASE
	public String title() {
		return title;
	}
	
	// RENAMED METHOD TO LOWER CASE
	public boolean available() {
		return state == STATE.AVAILABLE;
	}

	// RENAMED METHOD TO LOWER CASE
	public boolean onloan() {
		return state == STATE.ONLOAN;
	}

	// RENAMED METHOD TO LOWER CASE
	public boolean damaged() {
		return state == STATE.DAMAGED;
	}

	// RENAMED METHOD TO LOWER CASE
	public void borrow() {
		if (state.equals(STATE.AVAILABLE)) {
			state = STATE.ONLOAN;
		}
		else {
			throw new RuntimeException(String.format("Book: cannot borrow while book is in state: %s", state));
		}
		
	}

	// RENAMED METHOD TO LOWER CASE
	public void returns(boolean DAMAGED) {
		if (state.equals(STATE.ONLOAN)) {
			if (DAMAGED) {
				state = STATE.DAMAGED;
			}
			else {
				state = STATE.AVAILABLE;
			}
		}
		else {
			throw new RuntimeException(String.format("Book: cannot Return while book is in state: %s", state));
		}		
	}

	// RENAMED METHOD TO LOWER CASE
	public void repair() {
		if (state.equals(STATE.DAMAGED)) {
			state = STATE.AVAILABLE;
		}
		else {
			throw new RuntimeException(String.format("Book: cannot repair while book is in state: %s", state));
		}
	}
}