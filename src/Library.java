import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//file ready for static review

@SuppressWarnings("serial")
public class Library implements Serializable {
	
	private static final String LIBRARY_FILE = "library.obj";
	private static final int LOAN_LIMIT = 2;
	private static final int LOAN_PERIOD = 2;
	private static final double FINE_PER_DAY = 1.0;
	private static final double MAX_FINES_OWED = 5.0;
	private static final double DAMAGE_FEE = 2.0;
	
	private static Library self;
	// RENAME VARIABLES
	private int bookID;
	private int memberID;
	private int loanID;
	private Date loadDate;
	
	private Map<Integer, Book> catalog;
	private Map<Integer, Member> members;
	private Map<Integer, loan> loans;
	private Map<Integer, loan> currentLoans;
	private Map<Integer, Book> damagedBooks;
	

	private Library() {
		catalog = new HashMap<>();
		members = new HashMap<>();
		loans = new HashMap<>();
		currentLoans = new HashMap<>();
		damagedBooks = new HashMap<>();		
		// UPDATED VARIABLES
		bookID = 1;
		memberID = 1;		
		loanID = 1;		
	}

	
	public static synchronized Library INSTANCE() {		
		if (self == null) {
			Path path = Paths.get(LIBRARY_FILE);			
			if (Files.exists(path)) {	
				try (ObjectInputStream lof = new ObjectInputStream(new FileInputStream(LIBRARY_FILE));) {
					self = (Library) lof.readObject();
					Calendar.getInstance().setDate(self.loadDate);
					lof.close();
				}
				catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
			else self = new Library();
		}
		return self;
	}

	
	public static synchronized void SAVE() {
		if (self != null) {
			self.loadDate = Calendar.getInstance().date();
			try (ObjectOutputStream lof = new ObjectOutputStream(new FileOutputStream(LIBRARY_FILE));) {
				lof.writeObject(self);
				lof.flush();
				lof.close();	
			}
			catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
	}
	
	// RENAME METHOD & UPDATED bookID
	public int bookID() {
		return bookID;
	}
	
	// RENAME METHOD & UPDATED memberID
	public int memberemberID() {
		return memberID;
	}
	
	// RENAME METHOD & UPDATED bookID
	private int nextBookID() {
		return bookID++;
	}
	
	// RENAME METHOD & UPDATED memberID
	private int nextMemberID() {
		return memberID++;
	}
	
	// RENAME METHOD & UPDATED loanID
	private int nextLoanID() {
		return loanID++;
	}

	
	public List<Member> Members() {		
		return new ArrayList<Member>(members.values()); 
	}


	public List<Book> Books() {		
		return new ArrayList<Book>(catalog.values()); 
	}


	public List<loan> CurrentLoans() {
		return new ArrayList<loan>(currentLoans.values());
	}


	public Member Add_mem(String lastName, String firstName, String email, int phoneNo) {		
		Member member = new Member(lastName, firstName, email, phoneNo, nextMemberID());
		members.put(member.getId(), member);		
		return member;
	}

	
	public Book Add_book(String a, String t, String c) {		
		Book b = new Book(a, t, c, nextBookID());
		catalog.put(b.id(), b);		
		return b;
	}

	
	public Member getMember(int memberId) {
		if (members.containsKey(memberId)) 
			return members.get(memberId);
		return null;
	}

	
	public Book Book(int bookId) {
		if (catalog.containsKey(bookId)) 
			return catalog.get(bookId);		
		return null;
	}

	
	public int loanLimit() {
		return LOAN_LIMIT;
	}

	
	public boolean memberCanBorrow(Member member) {		
		if (member.getNumberOfCurrentLoans() == LOAN_LIMIT ) {
			return false;
		}	
		if (member.getFinesOwed() >= MAX_FINES_OWED) {
			return false;
		}
		for (loan loan : member.getLoans()) {
			if (loan.isOverDue()) {
				return false;
			}
		}	
		return true;
	}

	
	public int loansRemainingForMember(Member member) {		
		return LOAN_LIMIT - member.getNumberOfCurrentLoans();
	}

	
	public loan issueLoan(Book book, Member member) {
		Date dueDate = Calendar.getInstance().getDueDate(LOAN_PERIOD);
		loan loan = new loan( bookID, book, member, dueDate);
		member.takeOutLoan(loan);
		book.borrow();
		loans.put(loan.getId(), loan);
		currentLoans.put(book.id(), loan);
		return loan;
	}
	
	
	public loan getLoanByBookId(int bookId) {
		if (currentLoans.containsKey(bookId)) {
			return currentLoans.get(bookId);
		}
		return null;
	}

	
	public double calculateOverDueFine(loan loan) {
		if (loan.isOverDue()) {
			long daysOverDue = Calendar.getInstance().getDaysDifference(loan.getDueDate());
			double fine = daysOverDue * FINE_PER_DAY;
			return fine;
		}
		return 0.0;		
	}


	public void dischargeLoan(loan currentLoan, boolean isDamaged) {
		Member member = currentLoan.Member();
		Book book  = currentLoan.Book();
		
		double overDueFine = calculateOverDueFine(currentLoan);
		member.addFine(overDueFine);	
		
		member.dischargeLoan(currentLoan);
		book.returns(isDamaged);
		if (isDamaged) {
			member.addFine(DAMAGE_FEE);
			damagedBooks.put(book.id(), book);
		}
		currentLoan.Loan();
		currentLoans.remove(book.id());
	}


	public void checkCurrentLoans() {
		for (loan loan : currentLoans.values()) {
			loan.checkOverDue();
		}		
	}


	public void repairBook(Book currentBook) {
		if (damagedBooks.containsKey(currentBook.id())) {
			currentBook.repair();
			damagedBooks.remove(currentBook.id());
		}
		else {
			throw new RuntimeException("Library: repairBook: book is not damaged");
		}
	}
}